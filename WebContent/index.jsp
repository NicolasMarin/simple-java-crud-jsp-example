<%@page import="java.sql.Statement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="css/estilos.css" rel="stylesheet" type="text/css">
	<title>Insert title here</title>
</head>
<body>
	<%
	//conectando a la base de datos
	Connection con;
	String url = "jdbc:mysql://localhost:3306/registro";
	String user = "root";
	String pass = "mysqlpass";
	con = DriverManager.getConnection(url, user, pass);
	
	//Listamos datos de la tabla usuario
	Statement smt;
	ResultSet rs;
	smt = con.createStatement();
	rs = smt.executeQuery("select * from usuario");
	
	//Creamos la tabla
	%>
	<div class="container">
		<h1>Lista de Resgistros</h1>
		<hr>
		<br>
		<div class="container buscar" ><!-- class="container buscar" -->
			<a class="btn btn-success" href="Agregar.jsp">Nuevo Registro</a> <!-- class="btn btn-success btn-lg" -->
			<form class="form" action="" ><!-- class="form" -->
				<input  type="text" name="txtBuscar"><!-- class="form-control" -->
				<input  type="submit" value="Buscar"><!-- class="btn btn-lg" class="btn btn" -->
			</form>
			<%
			String nomBuscar = request.getParameter("txtBuscar");
			if(nomBuscar != null){
				smt = con.createStatement();
				rs = smt.executeQuery("select * from usuario where Nombres LIKE '%" + nomBuscar + "%';");
			}else{
				System.err.print("Error");
			}
			%>
		</div>
		<br>
		<table class="table table-bordered">
			<tr>
				<th class="text-center">ID</th>
				<th class="text-center">DNI</th>
				<th class="text-center">Nombre</th>
				<th class="text-center">Acciones</th>
			</tr>
			<%
				while(rs.next()){
			%>
			<tr>
				<td class="text-center"><%=rs.getInt(1) %></td>
				<td class="text-center"><%=rs.getString(2) %></td>
				<td class="text-center"><%=rs.getString(3) %></td>
				<td class="text-center">
					<a href="Editar.jsp?id=<%=rs.getInt(1) %>" class="btn btn-warning btn-sm">Editar</a>
					<a href="Delete.jsp?id=<%=rs.getInt(1) %>" class="btn btn-danger btn-sm">Eliminar</a>
				</td>
			</tr>
			<% } %>
		</table>
	</div>
	
</body>
</html>