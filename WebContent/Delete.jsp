<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Insert title here</title>
	</head>
	<body>
		<%
		//conectando a la base de datos
		Connection con;
		String url = "jdbc:mysql://localhost:3306/registro";
		String Driver = "com.mysql.jdbc.Driver";
		String user = "root";
		String pass = "mysqlpass";
		con = DriverManager.getConnection(url, user, pass);
		
		//Listamos datos de la tabla usuario de la fila seleccionada
		PreparedStatement ps;
		int id = Integer.parseInt(request.getParameter("id"));
		ps = con.prepareStatement("delete from usuario where Id=" + id);
		ps.executeUpdate();
		response.sendRedirect("index.jsp");
		%>
	</body>
</html>