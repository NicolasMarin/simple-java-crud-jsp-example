drop database if exists registro;
create database registro;
use registro;
drop table if exists usuario;
create table usuario (Id int NOT NULL AUTO_INCREMENT, DNI varchar(10), Nombres varchar(30), PRIMARY KEY (Id));
insert into usuario (DNI, Nombres) values ("11111111", "Usuario 1");
insert into usuario (DNI, Nombres) values ("22222222", "Usuario 2");
insert into usuario (DNI, Nombres) values ("33333333", "Usuario 3");