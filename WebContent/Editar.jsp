<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<title>Insert title here</title>
	</head>
	<body>
		<%
		String url = "jdbc:mysql://localhost:3306/registro";
		String user = "root";
		String pass = "mysqlpass";
		Connection con = DriverManager.getConnection(url, user, pass);
		
		//Listamos datos de la tabla usuario de la fila seleccionada
		int id = Integer.parseInt(request.getParameter("id"));
		PreparedStatement ps = con.prepareStatement("select * from usuario where Id=" + id);
		ResultSet rs = ps.executeQuery();
		//Creamos la tabla
		while(rs.next()){%>
			<div class="container">
				<h1>Modificar Registro</h1>
				<hr>
				<form action="" method="post" class="form-control" style="width: 500px; height: 400px;">
					ID:
					<input type="text" readonly="readonly" class="form-control" value="<%=rs.getInt("Id")%>"/>
					DNI:
					<input type="text" name="txtDNI" class="form-control" value="<%=rs.getString("DNI")%>"/>
					Nombre:
					<input type="text" name="txtNombre" class="form-control" value="<%=rs.getString("Nombres")%>"/>
					<br>
					<input type="submit" value="Guardar" class="btn btn-primary btn-lg"/>
					<br><br>
					<a href="index.jsp">Regresame</a>
				</form>
			</div>
		<% } %>
	</body>
</html>
<%
	//conectando a la base de datos
	String dni = request.getParameter("txtDNI");
	String nombre = request.getParameter("txtNombre");
	if(nombre != null && dni != null){
		ps = con.prepareStatement("update usuario set DNI='" + dni + "', Nombres='" + nombre + "' where Id = " + id);
		ps.executeUpdate();
		response.sendRedirect("index.jsp");
	}%>