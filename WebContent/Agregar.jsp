<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<title>Agregar Registro</title>
	</head>
	<body>
		<div class="container">
			<h1>Agregar Registro</h1>
			<hr>
			<form action="" method="post" class="form-control" style="width: 500px; height: 400px;">
				DNI:
				<input type="text" name="txtDNI" class="form-control"/>
				Nombre:
				<input type="text" name="txtNombre" class="form-control"/>
				<br>
				<input type="submit" value="Guardar" class="btn btn-primary btn-lg"/>
				<br><br>
				<a href="index.jsp">Regresame</a>
			</form>
		</div>
	</body>
</html>
<%
	//conectando a la base de datos
	Connection con;
	String url = "jdbc:mysql://localhost:3306/registro";
	String user = "root";
	String pass = "mysqlpass";
	con = DriverManager.getConnection(url, user, pass);
	
	String dni = request.getParameter("txtDNI");
	String nombre = request.getParameter("txtNombre");
	if(nombre != null && dni != null){
		PreparedStatement ps = con.prepareStatement("insert into usuario(DNI, Nombres) values('" + dni + "', '" + nombre + "');");
		ps.executeUpdate();
		response.sendRedirect("index.jsp");
	}%>